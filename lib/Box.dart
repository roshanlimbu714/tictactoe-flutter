import 'package:flutter/material.dart';
class Box extends StatelessWidget {
  Function insertInput;
  int id;
  String input;
  Box(this.insertInput,this.id, this.input);
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: (){
        insertInput(id);
      },
      child: Container(
          margin: EdgeInsets.all(4),
          height: 120,
          width: 120,
          color: Colors.red,
          child: Center(
            child: Text(input, style: TextStyle(
              fontSize: 48,
              color:Colors.white
            ),),
          ),
      ),
    );
  }
}
