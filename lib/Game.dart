import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tictactoe/Box.dart';

class Game extends StatefulWidget {
  const Game({Key? key}) : super(key: key);

  @override
  _GameState createState() => _GameState();
}

class _GameState extends State<Game> {
  List<String> inputs = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];
  String currentPlayer = 'p1';
  List<int> player1Inp = [];
  List<int> player2Inp = [];
  List<List<int>> winningConditions = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7],
  ];

  void insertInput(id) {
    if (inputs[id-1] == '') {
      if (currentPlayer == 'p1') {
        setState(() {
          inputs[id - 1] = 'O';
          player1Inp.add(id);
          currentPlayer = 'p2';
        });
      } else {
        setState(() {
          inputs[id - 1] = 'X';
          currentPlayer = 'p1';
          player2Inp.add(id);
        });
      }
    }
    checkIfWon();
  }

  void checkIfWon() {
    bool didWin = false;
    for (var i=0; i <winningConditions.length; i++) {
      var inps = currentPlayer == 'p1' ? player1Inp : player2Inp;
      for (var j=0; j < winningConditions[i].length; j++) {
        didWin = inps.contains(winningConditions[i][j]);
        print(didWin);
      }
      if(didWin){
        print(currentPlayer + ' won');
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tictactoe')),
      body: Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Box(insertInput, 1, inputs[0]),
                Box(insertInput, 2, inputs[1]),
                Box(insertInput, 3, inputs[2]),
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Box(insertInput, 4, inputs[3]),
                Box(insertInput, 5, inputs[4]),
                Box(insertInput, 6, inputs[5]),
              ]),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Box(insertInput, 7, inputs[6]),
                Box(insertInput, 8, inputs[7]),
                Box(insertInput, 9, inputs[8]),
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
